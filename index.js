const Discord = require("discord.js");
const client = new Discord.Client();

const discordServer = "308049236606582787"; //uagServer
const victim = "247625024779845632"; //Collin
const countdownBot = "583048410194640897"; //next session in

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

//whenever presence changes check if it is 30 mins before the op
client.on("presenceUpdate", (oldMember, newMember) => {
  if (newMember.id === countdownBot) {
    let newPresence = newMember.presence,
      oldPresence = oldMember.presence,
      countdown,
      countdownNum;

    if (newPresence.game !== null && newPresence.game !== oldPresence.game) {
      countdown = newPresence.game.name;
      //only get the numbers from the presence
      //if presence is smaller than 30 then send a single message without interval, so that it does not cross with other possible messages, otherwise it might send too often
      console.log(countdown);
      countdownNum = parseInt(countdown.replace(/[^0-9]/g, ""), 10);
    }

    // send a message every minute if 30 mins before the op, stop when countdown expires
    if (countdownNum <= 30) {
      sendMessage();
    } else if (countdown === "UNDEFINED") {
      finalMessage();
    }
  }
});

//sends a message to the victim
function sendMessage() {
  let uagServer = client.guilds.get(discordServer);
  uagServer.fetchMember(victim).then(user => {
    // if the user is not in a voice channel then start sending shit
    if (user.voiceChannel === undefined) {
      user.send("Get your ass in here, the op is starting soon");
    }
  });
}

function finalMessage() {
  let uagServer = client.guilds.get(discordServer);
  uagServer.fetchMember(victim).then(user => {
    if (user.voiceChannel === undefined) {
      user.send("Final Warning, we are all judging you if you don't join us");
    }
  });
}

client.login("TOKEN");
